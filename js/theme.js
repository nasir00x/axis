// Affix
$('.navbar-default').affix({
    offset: {
        top: 200,
        bottom: function () {
            return (this.bottom = $('.footer').outerHeight(true))
        }
    }
});

//Search
$('.search-modal').magnificPopup({
    type: 'inline',
    preloader: true,
    focus: '#search-input',
    mainClass: 'mfp-with-zoom',
    modal: true
});
$(document).on('click', '.search-modal-dismiss', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
});

// Slider
$('.home-slider, .design-carousel, .civil-works').owlCarousel({
    items: 1,
    margin: 0,
    loop: true, 
    autoplay: true,
    dots: true
});

// Slider
$('.awards-carousel').owlCarousel({
    items: 2,
    margin: 30,
    loop: true, 
    autoplay: true,
    dots: true
});

if ( $('.isotope-gallery').length ){
			
    $(".isotope-gallery").isotope({
        itemSelector: ".gallery-item",
        layoutMode: 'masonry',
        masonry: {
            columnWidth: '.grid-sizer'
        }
    });

    // Add isotope click function
    $(".gallery-filter li").on('click',function(){
        $(".gallery-filter li").removeClass("active");
        $(this).addClass("active");

        var selector = $(this).attr("data-filter");
        $(".filterable-gallery").isotope({
            filter: selector
        })
    })                
}

if ( $('.print-page').length ){			
    $(".print-page").on('click', function(){
		window.print();
		return false
	})
}

if ( $('#mapBox').length ){
	var $lat = $('#mapBox').data('lat');
	var $lon = $('#mapBox').data('lon');
	var $zoom = $('#mapBox').data('zoom');
	var $marker = $('#mapBox').data('marker');
	var $info = $('#mapBox').data('info');
	var map = new GMaps({
		el: '#mapBox',
		lat: $lat,
		lng: $lon,
		scrollwheel: false,
		scaleControl: true,
		streetViewControl: false,
		panControl: true,
		disableDoubleClickZoom: true,
		mapTypeControl: false,
		zoom: $zoom,
		styles: [
			{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},          
			{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},
			{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},
			{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},
			{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},
			{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},
			{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},
			{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},
			{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},
			{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},
			{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}
		]
	});

	map.addMarker({
		lat: $lat,
		lng: $lon,
		icon: $marker,				
		infoWindow: {
			content: $info
		}
	})
}